import discord

from services.guild_service import GuildService
from services.lyrics_service import LyricsService
from services.message_service import MessageService
from services.music_service import MusicService

from discord.ext import commands

class MusicCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def join(self, ctx):
        guild = GuildService.get_or_add_guild(ctx.message.guild.id)
        guild.set_text_channel(ctx.message.channel)
        await MusicService.join(guild, ctx.author.voice, ctx.voice_client)

    @commands.command()
    async def leave(self, ctx):
        guild = GuildService.get_or_add_guild(ctx.message.guild.id)
        guild.set_text_channel(ctx.message.channel)
        await MusicService.disconnect(guild)

    @commands.command()
    async def play(self, ctx, *args):
        guild = GuildService.get_or_add_guild(ctx.message.guild.id)
        guild.set_text_channel(ctx.message.channel)

        # Join a voice channel if bot not in any voice channel
        if ctx.voice_client is None:
            await MusicService.join(guild, ctx.author.voice, ctx.voice_client)

        # Search song and add to queue
        keyword = " ".join(args)
        await MusicService.play(guild, keyword)

    @commands.command()
    async def pause(self, ctx):
        guild = GuildService.get_or_add_guild(ctx.message.guild.id)
        guild.set_text_channel(ctx.message.channel)
        await MusicService.pause(guild)

    @commands.command()
    async def resume(self, ctx):
        guild = GuildService.get_or_add_guild(ctx.message.guild.id)
        guild.set_text_channel(ctx.message.channel)
        await MusicService.resume(guild)

    @commands.command()
    async def skip(self, ctx):
        guild = GuildService.get_or_add_guild(ctx.message.guild.id)
        guild.set_text_channel(ctx.message.channel)
        await MusicService.skip(guild)

    @commands.command()
    async def queue(self, ctx):
        guild = GuildService.get_or_add_guild(ctx.message.guild.id)
        guild.set_text_channel(ctx.message.channel)
        await MusicService.get_queue(guild)

    @commands.command()
    async def loop(self, ctx):
        guild = GuildService.get_or_add_guild(ctx.message.guild.id)
        guild.set_text_channel(ctx.message.channel)
        await MusicService.loop(guild)

    @commands.command()
    async def clear(self, ctx):
        guild = GuildService.get_or_add_guild(ctx.message.guild.id)
        guild.set_text_channel(ctx.message.channel)
        await MusicService.clear_queue(guild)

    @commands.command()
    async def lyrics(self, ctx):
        guild = GuildService.get_or_add_guild(ctx.message.guild.id)
        guild.set_text_channel(ctx.message.channel)
        await LyricsService.get_current_lyrics(guild)

def setup(bot):
    bot.add_cog(MusicCommands(bot))
