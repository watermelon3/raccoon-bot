import discord
import requests

from discord.ext import commands

TRIVIA_API_URL = "https://opentdb.com/api.php?"

class TriviaCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.answer = None

    # Request a multiple choice question and returns a dictionary
    # Return format {"question": <question>, "answer": <answer (A, B, C, ...)>, "choices": <randomized choice>}
    def mutliple_choice(self):
        pass

    # Request a true false question and returns a dictionary
    # Return format {"question": <question>, "answer": <answer>}
    def true_false(self):
        pass

    # Request a regular question and returns a dictionary
    # Return format {"question": <question>, "answer": <answer>}
    # Notes: since the API doesn't provide regular question, regular question can be obtained by removing the choice from multiple choice question
    def regular(self):
        pass

    @commands.command()
    async def trivia(self, ctx, *args):
        pass

def setup(bot):
    bot.add_cog(TriviaCommands(bot))
