from models.music_player import MusicPlayer

class Guild():
    def __init__(self, id):
        self.id = id
        self.text_channel = None
        self.music_player = MusicPlayer()

    def get_id(self):
        return self.id

    def get_music_player(self):
        return self.music_player

    def get_text_channel(self):
        return self.text_channel

    def set_text_channel(self, text_channel):
        self.text_channel = text_channel
        return self.text_channel