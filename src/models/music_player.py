class MusicPlayer():
    def __init__(self):
        # Song queue is a list of dict, dict format {"source": "...", "title": ...}
        self.song_queue = []
        self.current_index = 0

        self.is_looping = False
        self.is_playing = False

        self.voice_client = None

    def get_is_playing(self):
        return self.is_playing
    
    def set_is_playing(self, value):
        self.is_playing = value
        return self.is_playing

    def get_queue_length(self):
        return len(self.song_queue)

    def get_queue(self):
        return self.song_queue

    def add_queue(self, song):
        self.song_queue.append(song)

    def get_current_index(self):
        return self.current_index

    def set_current_index(self, index):
        self.current_index = index
        return self.current_index

    def increment_index(self):
        if self.get_queue_length() > self.get_current_index() + 1:
            self.current_index += 1
        elif self.is_looping:
            self.current_index = 0
        else:
            self.current_index += 1
            
        return self.current_index

    def decrement_index(self):
        self.current_index -= 1
        return self.current_index

    def get_current_song(self):
        return self.song_queue[self.get_current_index()]

    def change_loop(self):
        self.is_looping = not self.is_looping
        return self.is_looping

    def get_voice_client(self):
        return self.voice_client

    def set_voice_client(self, voice_client):
        self.voice_client = voice_client
        return self.voice_client

    def clear_queue(self):
        self.song_queue.clear()
        self.voice_client.stop()
        self.is_playing = False

    async def disconnect(self):
        self.clear_queue()
        await self.voice_client.disconnect()

    def pause(self):
        self.voice_client.pause()

    def resume(self):
        self.voice_client.resume()

    def stop(self):
        self.voice_client.stop()