# bot.py
import os
import random
import discord

from cogs import music, test

from discord.ext import commands
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.environ.get("PRODUCTION_TOKEN")
PREFIX = os.environ.get("PRODUCTION_PREFIX")

if TOKEN == None:
    TOKEN = os.getenv("DISCORD_TOKEN")
    PREFIX = os.getenv("COMMAND_PREFIX")

bot = commands.Bot(command_prefix=PREFIX)
cogs = [music, test]

for cog in cogs:
    cog.setup(bot)

@bot.event
async def on_ready():
    await bot.change_presence(activity=discord.Game(name="with You"))
    print(f'{bot.user} has connected to Discord!')

@bot.command(name='99', help='Responds with a random quote from Brooklyn 99')
async def nine_nine(ctx):
    brooklyn_99_quotes = [
        'I\'m the human form of the 💯 emoji.',
        'Bingpot!',
        (
            'Cool. Cool cool cool cool cool cool cool, '
            'no doubt no doubt no doubt no doubt.'
        ),
    ]

    response = random.choice(brooklyn_99_quotes)
    await ctx.send(response)

@bot.command(name='dice', help='Simulates rolling dice.')
async def roll(ctx, number_of_dice: int, number_of_sides: int):
    if number_of_dice and number_of_sides:
        dice = [
            str(random.choice(range(1, number_of_sides + 1)))
            for _ in range(number_of_dice)
        ]
        await ctx.send(', '.join(dice))
    else:
        await ctx.send("The correct format is \'#dice <number of dice> <number of sides>\' and numbers can't be zero")

bot.run(TOKEN)