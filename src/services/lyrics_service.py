import requests

from bs4 import BeautifulSoup

from services.message_service import MessageService

class LyricsService:
    URL = 'https://www.google.com/search?q='

    @staticmethod
    async def get_current_lyrics(guild):
        current_song = guild.get_music_player().get_current_song()

        try:
            title = current_song['title']
            title = title.replace('Official', '')
            title = title.replace('Video', '')
            title = title.replace('| Music | - Lagu Rohani', '')

            search_url = LyricsService.URL + title + " lyrics"
            
            lyrics = requests.get(search_url).text
            clean_lyrics = str(BeautifulSoup(lyrics).find_all("div", class_="BNeawe tAd8D AP7Wnd")[3])
            clean_lyrics = clean_lyrics[33:-6]

            MessageService.send(guild.get_text_channel(), '>>> ' + clean_lyrics)
        except:
            try:
                title = current_song['query']
                search_url = LyricsService.URL + title + " lyrics"

                lyrics = requests.get(search_url).text
                clean_lyrics = str(BeautifulSoup(lyrics).find_all("div", class_="BNeawe tAd8D AP7Wnd")[3])
                clean_lyrics = clean_lyrics[33:-6]

                MessageService.send(guild.get_text_channel(), '>>> ' + clean_lyrics)
            except:
                MessageService.send(guild.get_text_channel(), '>>> Lyrics not found')