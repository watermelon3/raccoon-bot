import discord
import youtube_dl

from models.music_player import MusicPlayer
from services.message_service import MessageService

class MusicService:
    YDL_OPTIONS = {
        "format": "bestaudio/best",
        "extractaudio": True,
        "noplaylist": True
    }

    FFMPEG_OPTIONS = {
        "before_options": "-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5",
        "options": "-vn"
    }
        
    # Search keyword on youtube and return url ("source") and title ("title") of the first result as dictionary
    @staticmethod
    def search_youtube(keyword):
        with youtube_dl.YoutubeDL(MusicService.YDL_OPTIONS) as ydl:
            try:
                info = ydl.extract_info("ytsearch:%s" % keyword, download=False)["entries"][0]
                return {
                    "source": info["formats"][0]["url"],
                    "title": info["title"]
                }
            except:
                return False

    @staticmethod
    def play_next(guild):
        music_player = guild.get_music_player()
        music_player.increment_index()

        if music_player.get_queue_length() > music_player.get_current_index():
            music_player.set_is_playing(True)

            song = music_player.get_current_song()
            source = discord.FFmpegPCMAudio(song["source"], **MusicService.FFMPEG_OPTIONS)

            # Play next song when song done or stopped
            music_player.voice_client.play(source, after = lambda a : MusicService.play_next(guild))

            # Send message
            MessageService.send(guild.get_text_channel(), "Now playing %s" % song["title"])

        else:
            music_player.set_is_playing(False)

    @staticmethod
    async def disconnect(guild):
        music_player = guild.get_music_player()
        await music_player.disconnect()

        MessageService.send(guild.get_text_channel(), "Reset music player")

    @staticmethod
    async def clear_queue(guild):
        music_player = guild.get_music_player()
        music_player.clear_queue()

        MessageService.send(guild.get_text_channel(), "Queue cleared")

    @staticmethod
    async def join(guild, author_voice_state, bot_voice_client):
        music_player = guild.get_music_player()

        if author_voice_state is None:
            # Author have to be in a voice channel
            MessageService.send(guild.get_text_channel(), "You need to join a voice channel")
        else:
            voice_channel = author_voice_state.channel

            # Bot have not joined any voice channel
            if bot_voice_client is None:
                voice_client = await voice_channel.connect()
                music_player.set_voice_client(voice_client)
                MessageService.send(guild.get_text_channel(), 'Joined voice channel %s' % voice_channel)

            # If bot already in a voice channel, move bot to author's voice channel
            else:
                voice_client = await bot_voice_client.move_to(voice_channel)
                music_player.set_voice_client(voice_client)
                MessageService.send(guild.get_text_channel(), 'Moved to voice channel %s' % voice_channel)

    @staticmethod
    async def play(guild, keyword):
        music_player = guild.get_music_player()

        info = MusicService.search_youtube(keyword)

        # If queue is empty, reset song index
        if music_player.get_queue_length() == 0:
            music_player.set_current_index(0)

        # If search doesn't return False (no exeception occured)
        if info:
            info['query'] = keyword
            music_player.add_queue(info)

            if not music_player.get_is_playing():
                music_player.decrement_index()
                MusicService.play_next(guild)
            else:
                MessageService.send(guild.get_text_channel(), 'Queued %s' % info['title'])

        else:
            MessageService.send(guild.get_text_channel(), 'Failed to fetch song')

    @staticmethod
    async def pause(guild):
        guild.get_music_player().pause()
        MessageService.send(guild.get_text_channel(), 'Paused')

    @staticmethod
    async def resume(guild):
        guild.get_music_player().resume()
        MessageService.send(guild.get_text_channel(), 'Resumed')

    @staticmethod
    async def skip(guild):
        guild.get_music_player().stop()

    @staticmethod
    async def get_queue(guild):
        music_player = guild.get_music_player()
        song_queue = music_player.get_queue()

        response = ">>> "

        for i in range(len(song_queue)):
            if i == music_player.get_current_index() and music_player.is_playing:
                response += "%d. %s (%s)\n" % (i + 1, song_queue[i]["title"], "Paused" if music_player.get_voice_client().is_paused() else "Playing")
            else:
                response += "%d. %s\n" % (i + 1, song_queue[i]["title"])
        
        if response == ">>> ":
            MessageService.send(guild.get_text_channel(), 'The queue is empty')
        else:
            MessageService.send(guild.get_text_channel(), response)

    @staticmethod
    async def loop(guild):
        music_player = guild.get_music_player()
        is_looping = music_player.change_loop()

        # Reset to first song if looping enabled and audio not playing
        if (not music_player.get_is_playing()) and is_looping:
            music_player.decrement_index()
            MusicService.play_next(guild)

        MessageService.send(guild.get_text_channel(), "Looping is set to %r" % is_looping)
