from models.guild import Guild

class GuildService:
    # Guilds is a dict, dict format {"guild_id": Guild}
    guilds = dict()

    @staticmethod
    def get_or_add_guild(guild_id):
        if guild_id in GuildService.guilds:
            return GuildService.guilds[guild_id]
        else:
            new_guild = Guild(guild_id)
            GuildService.guilds[guild_id] = new_guild
            return GuildService.guilds[guild_id]