# Raccoon Bot

## Project initialization (Windows)
1. Open cmd and cd to your project directory
2. Run 'virtualenv env', you can replace env with any name you want
3. Activate virtual environment by running 'env\Scripts\Activate'
4. Install necessary packages, in this case:
    - discord
    - python-dotenv
5. You can do number 4 by running:
    - 'pip install -U discord.py'
    - 'pip install -U python-dotenv'
6. Clone this repository
7. Done!

## Working on this project
1. Make your own branch if you haven't done so
2. Code on your own branch
3. Test your code before commiting
4. Commit your works with an UNDERSTANDABLE COMMIT NAME
6. Done!

## Release your feature
1. Make merge request from your branch to staging branch
2. Complete the merge request on your own or ask someone to do it
3. Make merge request from staging branch to master branch
4. Describe your changes as simple as possible on the commit name section
5. Complete the merge request on your own or ask someone to do it
6. Done!